" Use Vim settings instead of Vi
" This must be the first setting!
set nocompatible

" General {{{

set history=1000                       " Increase :cmdline history
set showcmd                            " Show incomplete commands
set showmode                           " Show current mode
set noerrorbells                       " Disable audio errors
set novisualbell                       " Disable visual errors
set autoread                           " Reload files changed outside of vim
set hidden                             " Allow buffers in the background
set mouse=a                            " Enable GPM support
set viminfo+='1000,n~/.vim/viminfo'    " Disable .viminfo file

" Prefer utf-8 encoding
scriptencoding utf-8
set encoding=utf-8

" General }}}

" Syntax highlighting {{{

syntax on                              " Enable highlighting
filetype on                            " Determine hightlighting by filetype
filetype indent on                     " Determine indentation by filetype
filetype plugin on                     " Enable filetype plugin

" Syntax highlighting }}}

" Backups & session {{{

" Turn off backups
set nobackup                           " Disable tilda file backups
set nowritebackup                      " Disable write backups
set noswapfile                         " Disable swap files

" Allow undo across sessions where possible
if has ('persistent_undo') && isdirectory(expand('~').'/.vim/backups')
    silent !mkdir ~/.vim/backups > /dev/null 2>&1
    set undodir=~/.vim/backups
    set undofile
endif

" Ensure autocmd doesn't load things more than once
autocmd!

" Store last edit location
autocmd BufReadPost *
    \ if line("'\"") > 0 && line("'\"") <= line("$") |
    \     exe "normal! g`\"" |
    \ endif

" Backups & session }}}

" Formatting {{{

set autoindent                         " Auto-indent to previous line
set smartindent                        " Use smart indentation
set cindent                            " Strict indenting for C programs
set smarttab                           " Use smart tabbing
set shiftwidth=4                       " Set indentation size
set softtabstop=4                      " Set soft tabstop
set tabstop=4                          " Set tabstop
set nowrap                             " Disable line wrapping

" Autoindent pasted text
nnoremap p p=`]<C-o>
nnoremap P P=`]<C-o>

" Display tabs and trailing spaces visually
set list listchars=tab:\ \ ,trail:·

" Formatting }}}

" Folding {{{

" Use folding where possible
if has ('folding')
    set foldenable
    set foldmethod=marker
    set foldmarker={{{,}}}
    set foldcolumn=0
endif

" Folding }}}

" Scrolling {{{

set scrolloff=8                        " Start scrolling 8 lines from vertical margins
set sidescrolloff=15                   " Start scrolling 15 columns from horizontal margins
set sidescroll=1                       " How many columns to scroll at a time

" Scrolling }}}

" Searching {{{

set ignorecase                         " Case-insensitive search
set smartcase                          " Case-sensitive search if caps used
set hlsearch                           " Highlight searches by default
set incsearch                          " Search while typing

" Searching }}}

" Appearance {{{

set background=dark                    " Optimize colors for dark backgrounds
set shortmess=aTI                      " Display short messages
set showmatch                          " Highlight matching braces

" Statusline
set statusline=                        " Override default
set statusline+=%2*\ %f\ %m\ %r%*      " Show filename/path
set statusline+=%3*%=%*                " Set right-side status info after line
set statusline+=%4*%l/%L:%v%*          " Set <line>/<total>:<col>
set statusline+=%5*\ %*                " Set ending space

" Automatically resize splits when terminal is resized
autocmd VimResized * wincmd =

" Appearance }}}

" Misc helper scripts {{{

" Preview in browser
command PreviewWeb :!$BROWSER %<CR>

" Toggle line numbers with F2
nnoremap <silent> <F2> :set number!<CR>

" Toggle spellcheck with F11
map <F11> :set spell!<CR><Bar>:echo "Spell Check: " . strpart("OffOn", 3 * &spell, 3)<CR>

" Toggle paste mode with F12
nnoremap <F12> :set invpaste paste?<CR>
set pastetoggle=<F12>
set showmode

" Prompt for sudo on write
cmap w!! %!sudo tee > /dev/null %

" Handle stupid case errors
command! Q q
command! W w

" Mac keybindings
map <C-A> <Home>
imap <C-A> <C-O><Home>
map <C-E> <End>
imap <C-E> <C-O><End>

" Misc helper scripts }}}

" Setup plugins {{{

call plug#begin()                      " Get things started

Plug 'editorconfig/editorconfig-vim'   " Setup editorconfig plugin

call plug#end()                        " Initialize plugin system

" Run PlugInstjall if any plugins are missing
if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
	autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" Setup plugins }}}
